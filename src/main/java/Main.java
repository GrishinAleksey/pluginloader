import model.PluginManager;
import plugins.Plugin;

public class Main {
    public static void main(String[] args) {
        String classPath = "C:\\Users\\Алексей\\Desktop\\PluginLoader\\target\\classes\\plugins\\impl\\";
        PluginManager pluginManager = new PluginManager(classPath);
        Plugin plugin = pluginManager.load("Plugin2.class");
        plugin.load();
        plugin.run();
        plugin.unload();
    }
}
