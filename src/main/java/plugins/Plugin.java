package plugins;

public interface Plugin {

    int EXIT_SUCCESS = 0;

    void load();
    int run();
    void unload();

}
