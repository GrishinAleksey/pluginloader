package plugins.impl;

import plugins.Plugin;

public class Plugin2 implements Plugin{

    @Override
    public void load() {
        System.out.println("plugins.Plugin " + this.getClass() + " loading ...");
    }

    @Override
    public int run() {
        System.out.println("plugins.Plugin " + this.getClass() + " running ...");
        return Plugin.EXIT_SUCCESS;
    }

    @Override
    public void unload() {
        System.out.println("plugins.Plugin " + this.getClass() + " unloading ...");
    }
}
