package model;

import plugins.Plugin;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class PluginManager extends ClassLoader{

    private final String pluginRootDirectory;

    public PluginManager(String pluginRootDirectory) {
        this.pluginRootDirectory = pluginRootDirectory;
    }

    @Override
    public Class<?> findClass(String pluginName) {
        byte[] loadClassInByte = new byte[0];
        try {
            loadClassInByte = loadClassFromFile(pluginName);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return defineClass(null, loadClassInByte, 0, loadClassInByte.length);
    }

    private byte[] loadClassFromFile(String fileName) throws IOException {
        return Files.readAllBytes(Paths.get(fileName));
    }

    public Plugin load(String pluginClass) {
        try {
            return (Plugin) findClass(pluginRootDirectory + pluginClass).newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }
}
